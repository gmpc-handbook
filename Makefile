PARAMS=-a toc -a toclevels=3 -a date=$(shell date +%Y-%m-%d) -a numbered -d book


all: html

html: $(patsubst %.txt,%.html,$(wildcard *txt))

clean: 
	@$(RM) $(wildcard *.html)

%.html: %.txt
	asciidoc $(PARAMS) $<

.PHONY: clean
